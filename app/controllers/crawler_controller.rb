class CrawlerController < ApplicationController

  before_action :get_document, only: [:crawl]

  def crawl
    entries = @document.css('.media__link')
    links_news_arr = []
    entries.each do |entry|
      newz = entry.children.text.strip
      links_news_arr << { newz => entry.attributes['href'].value }
    end
    @resultant_hash = { "home_page" => links_news_arr }
  end

  private 

  def get_document
    require 'openssl'
    require 'open-uri'
    @document = Nokogiri::HTML(open(CRAWLER_URL).read)
  end
end
